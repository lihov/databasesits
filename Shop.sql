CREATE DATABASE shop;
USE shop;
CREATE TABLE categories
(
	id INT AUTO_INCREMENT,
    category VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO categories (category) VALUES ('Продукты'), ('Одежда'), ('Бытовая химия');

CREATE TABLE products
(
	id INT AUTO_INCREMENT,
    prodName VARCHAR(255) NOT NULL,
    price DECIMAL NOT NULL CHECK (price >= 0),
    discount DECIMAL DEFAULT 0 CHECK (discount >= 0),
    barcodeId BIGINT NOT NULL UNIQUE,
	prodCount INT NOT NULL,
    categoryId INT NOT NULL,
	PRIMARY KEY(id, barcodeId),
    FOREIGN KEY (categoryId) REFERENCES categories(id)
);

INSERT INTO products(prodName, price, barcodeId, prodCount, categoryId) VALUES('Колбаса копченая', 250, 123456781, 10, 1), 
('Колбаса вареная', 150, 123456782, 20, 1), ('Футболка синяя', 1000, 123456783, 5, 2), ('Штаны зеленые', 3000, 123456784, 3, 2),
('Моющее средство 1', 120, 123456785, 10, 3);

INSERT INTO products(prodName, price, barcodeId, prodCount, categoryId) VALUES ('Колбаса вареная', 120, 123456787, 5, 1);

UPDATE products SET price = price * 0.5, prodName = 'Колбаса вареная порченная' WHERE id = 2;

DELETE FROM products WHERE price >= 100;