USE world;
SELECT Name, District FROM city;
SELECT Name FROM city WHERE CountryCode = 'RUS' ORDER BY Name;
SELECT Name, CountryCode FROM city WHERE CountryCode IN ('GRC', 'PRT', 'ESP') ORDER BY CountryCode ASC, Name DESC;
SELECT Name, Population FROM city WHERE Population BETWEEN 300000 AND 500000 ORDER BY Population ASC;
SELECT Name FROM city WHERE Name LIKE 'A%' ORDER BY Name ASC;
SELECT Name FROM city WHERE Name LIKE '%A%' ORDER BY Name ASC;