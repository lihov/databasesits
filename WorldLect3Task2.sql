USE world;
SELECT AVG(Population) AS avgPopulation, CountryCode FROM city GROUP BY CountryCode ORDER BY avgPopulation DESC;
SELECT CountryCode, COUNT(*) AS cityCount FROM city GROUP BY CountryCode ORDER BY cityCount DESC;
SELECT CountryCode, COUNT(*) AS cityCount FROM city GROUP BY CountryCode HAVING COUNT(*) >= 2 ORDER BY cityCount DESC;
