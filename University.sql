DROP DATABASE IF EXISTS university;
CREATE DATABASE university;
USE university;

CREATE TABLE studentsGroup 
(
	id INT AUTO_INCREMENT,
    groupName VARCHAR(255) NOT NULL,
    tuitionForm ENUM('дневная', 'вечерняя', 'заочная') NOT NULL,
    specialty VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
);

INSERT INTO studentsGroup(groupName, tuitionForm, specialty) 
VALUES
	('АТЭ-11', 'дневная', 'Автоматизация в ТЭК'),
	('ТЭ-11', 'дневная', 'Теплоэнергетика'),
	('ТЭ-12', 'дневная', 'Теплоэнергетика'),
    ('ЭН-11', 'заочная', 'Электроэнергетика');

CREATE TABLE subjects
(
	id INT AUTO_INCREMENT,
    subjectName VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
);

INSERT INTO subjects(subjectName)
VALUES
	('Мат. анализ'),
	('Линейная алгебра'),
	('Физика'),
	('Техническая термодинамика'),
	('Тепломассообмен'),
	('Электротехника'),
	('Электроника'),
	('Дискрентная математика'),
	('ТАУ'),
	('Физкультура'),
	('Программиривание'),
	('Динамика жидкостей и газов'),
  	('Теория горения'),
	('Теория кипения'),
	('Химия'),
	('Философия'),
	('Эксплуатация АСУ');

CREATE TABLE subjectsPlan
(
	id INT AUTO_INCREMENT,
    subjectId INT NOT NULL,
    hours SMALLINT UNSIGNED NOT NULL,
    examinationForm ENUM('экзамен', 'зачет', 'дифф зачет') NOT NULL,
	termNumber SMALLINT UNSIGNED NOT NULL,
	PRIMARY KEY(id),
    FOREIGN KEY(subjectId) REFERENCES subjects(id)
);

INSERT INTO subjectsPlan(subjectId, hours, examinationForm, termNumber)
VALUES
	(1, 45, 'экзамен', 1),
    (1, 45, 'экзамен', 2),
	(2, 38, 'экзамен', 1),    
	(3, 52, 'экзамен', 1),    
	(3, 52, 'экзамен', 2),
    (3, 38, 'экзамен', 3),
    (4, 30, 'экзамен', 4),
    (5, 20, 'зачет', 5),
    (6, 52, 'зачет', 4),
    (6, 52, 'экзамен', 5),
    (7, 32, 'экзамен', 4),
    (8, 45, 'экзамен', 4),
    (9, 52, 'экзамен', 5),
    (9, 52, 'экзамен', 6),
    (10, 20, 'зачет', 1),
    (10, 20, 'зачет', 2),
    (10, 20, 'зачет', 3),
    (10, 20, 'зачет', 4),
    (11, 38, 'зачет', 3),
    (12, 0, 'зачет', 5),
    (12, 38, 'экзамен', 5),
    (13, 28, 'зачет', 6),
    (14, 28, 'зачет', 7),
    (15, 45, 'зачет', 1),
    (15, 45, 'экзамен', 2),
    (16, 28, 'экзамен', 2),
    (17, 28, 'экзамен', 7);

CREATE TABLE students
(
	id INT AUTO_INCREMENT,
    surname VARCHAR(255) NOT NULL,
    firstName VARCHAR(255) NOT NULL,
    patronymic VARCHAR(255),
    enterYear YEAR(4) NOT NULL,
    cardId BIGINT UNSIGNED NOT NULL UNIQUE,
    groupId INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(groupId) REFERENCES studentsGroup(id)
);

INSERT INTO students(surname, firstName, patronymic, enterYear, cardId, groupId)
VALUES
	('Иванов', 'Иван', 'Иванович', '2001', 123456711, 1),
	('Петров', 'Петр', 'Иванович', '2001', 123456712, 1),
	('Сидоров', 'Сигизмунд', NULL, '2001', 123456713, 1),
	('Коровин', 'Андрей', 'Андреич', '2001', 123456714, 2),
	('Быков', 'Владимир', 'Батькович', '2001', 123456715, 2),
	('Баранов', 'Владимир', 'Ильич', '2001', 123456716, 2),
    ('Гусев', 'Александр', 'Александрович', '2001', 123456717, 3),
    ('Кузнецов', 'Петр', 'Петрович', '2001', 123456718, 3),
    ('Чижиков', 'Дмитрий', 'Иванович', '2001', 123456720, 4);

CREATE TABLE progress
(
	id INT AUTO_INCREMENT,
    studentId INT NOT NULL,
    subjectPlanId INT NOT NULL,
    rating TINYINT UNSIGNED NOT NULL,
    passYear YEAR(4) NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(studentId) REFERENCES students(id),
    FOREIGN KEY(subjectPlanId) REFERENCES subjectsPlan(id)
);

INSERT INTO progress(studentId, subjectPlanId, rating, passYear)
VALUES
	(1, 1, 5, '2001'),
	(1, 2, 4, '2001'),
	(1, 3, 5, '2001'),
	(1, 4, 3, '2001'),
	(1, 5, 4, '2001'),
	(1, 6, 4, '2002'),
	(1, 7, 4, '2002'),
	(1, 8, 1, '2003'),
	(1, 9, 1, '2002'),
	(1, 10, 5, '2003'),
	(1, 11, 3, '2002'),
	(1, 12, 3, '2002'),
	(1, 13, 4, '2003'),
	(1, 14, 4, '2003'),
	(1, 15, 1, '2001'),
	(1, 16, 1, '2001'),
	(1, 17, 1, '2002'),
	(1, 18, 1, '2002'),
	(1, 19, 1, '2002'),
	(1, 20, 1, '2003'),
	(1, 21, 5, '2003'),
	(1, 22, 1, '2003'),
	(1, 23, 1, '2004'),
	(1, 24, 1, '2001'),
	(1, 25, 5, '2001'),
	(1, 26, 3, '2001'),
	(1, 27, 5, '2004'),

	(2, 1, 5, '2001'),
	(2, 2, 5, '2001'),
	(2, 3, 5, '2001'),
	(2, 4, 5, '2001'),
	(2, 5, 5, '2001'),
	(2, 6, 5, '2002'),
	(2, 7, 5, '2002'),
	(2, 8, 1, '2003'),
	(2, 9, 1, '2002'),
	(2, 10, 5, '2003'),
	(2, 11, 5, '2002'),
	(2, 12, 5, '2002'),
	(2, 13, 5, '2003'),
	(2, 14, 5, '2003'),
	(2, 15, 1, '2001'),
	(2, 16, 1, '2001'),
	(2, 17, 1, '2002'),
	(2, 18, 1, '2002'),
	(2, 19, 1, '2002'),
	(2, 20, 1, '2003'),
	(2, 21, 5, '2003'),
	(2, 22, 1, '2003'),
	(2, 23, 1, '2004'),
	(2, 24, 1, '2001'),
	(2, 25, 5, '2001'),
	(2, 26, 5, '2001'),
	(2, 27, 5, '2004'),

	(3, 1, 5, '2001'),
	(3, 2, 4, '2001'),
	(3, 3, 5, '2001'),
	(3, 4, 4, '2001'),
	(3, 5, 4, '2001'),
	(3, 6, 4, '2002'),
	(3, 7, 4, '2002'),
	(3, 8, 1, '2003'),
	(3, 9, 1, '2002'),
	(3, 10, 5, '2003'),
	(3, 11, 4, '2002'),
	(3, 12, 4, '2002'),
	(3, 13, 4, '2003'),
	(3, 14, 4, '2003'),
	(3, 15, 1, '2001'),
	(3, 16, 1, '2001'),
	(3, 17, 1, '2002'),
	(3, 18, 1, '2002'),
	(3, 19, 1, '2002'),
	(3, 20, 1, '2003'),
	(3, 21, 5, '2003'),
	(3, 22, 1, '2003'),
	(3, 23, 1, '2004'),
	(3, 24, 1, '2001'),
	(3, 25, 5, '2001'),
	(3, 26, 4, '2001'),
	(3, 27, 5, '2004'),

	(4, 1, 4, '2001'),
	(4, 2, 5, '2001'),
	(4, 3, 4, '2001'),
	(4, 4, 3, '2001'),
	(4, 5, 5, '2001'),
	(4, 6, 5, '2002'),
	(4, 7, 5, '2002'),
	(4, 8, 1, '2003'),
	(4, 9, 1, '2002'),
	(4, 10, 4, '2003'),
	(4, 11, 3, '2002'),
	(4, 12, 3, '2002'),
	(4, 13, 5, '2003'),
	(4, 14, 5, '2003'),
	(4, 15, 1, '2001'),
	(4, 16, 1, '2001'),
	(4, 17, 1, '2002'),
	(4, 18, 1, '2002'),
	(4, 19, 1, '2002'),
	(4, 20, 1, '2003'),
	(4, 21, 4, '2003'),
	(4, 22, 1, '2003'),
	(4, 23, 1, '2004'),
	(4, 24, 1, '2001'),
	(4, 25, 4, '2001'),
	(4, 26, 3, '2001'),
	(4, 27, 4, '2004'),

	(5, 1, 4, '2001'),
	(5, 2, 4, '2001'),
	(5, 3, 4, '2001'),
	(5, 4, 4, '2001'),
	(5, 5, 4, '2001'),
	(5, 6, 4, '2002'),
	(5, 7, 4, '2002'),
	(5, 8, 1, '2003'),
	(5, 9, 1, '2002'),
	(5, 10, 4, '2003'),
	(5, 11, 4, '2002'),
	(5, 12, 5, '2002'),
	(5, 13, 5, '2003'),
	(5, 14, 4, '2003'),
	(5, 15, 1, '2001'),
	(5, 16, 1, '2001'),
	(5, 17, 1, '2002'),
	(5, 18, 1, '2002'),
	(5, 19, 1, '2002'),
	(5, 20, 1, '2003'),
	(5, 21, 5, '2003'),
	(5, 22, 1, '2003'),
	(5, 23, 1, '2004'),
	(5, 24, 1, '2001'),
	(5, 25, 4, '2001'),
	(5, 26, 4, '2001'),
	(5, 27, 4, '2004'),

	(6, 1, 3, '2001'),
	(6, 2, 4, '2001'),
	(6, 3, 5, '2001'),
	(6, 4, 3, '2001'),
	(6, 5, 3, '2001'),
	(6, 6, 3, '2002'),
	(6, 7, 4, '2002'),
	(6, 8, 1, '2003'),
	(6, 9, 1, '2002'),
	(6, 10, 3, '2003'),
	(6, 11, 3, '2002'),
	(6, 12, 3, '2002'),
	(6, 13, 4, '2003'),
	(6, 14, 4, '2003'),
	(6, 15, 1, '2001'),
	(6, 16, 1, '2001'),
	(6, 17, 1, '2002'),
	(6, 18, 1, '2002'),
	(6, 19, 1, '2002'),
	(6, 20, 1, '2003'),
	(6, 21, 3, '2003'),
	(6, 22, 1, '2003'),
	(6, 23, 1, '2004'),
	(6, 24, 1, '2001'),
	(6, 25, 3, '2001'),
	(6, 26, 3, '2001'),
	(6, 27, 5, '2004'),

	(7, 1, 5, '2001'),
	(7, 2, 5, '2001'),
	(7, 3, 5, '2001'),
	(7, 4, 3, '2001'),
	(7, 5, 5, '2001'),
	(7, 6, 5, '2002'),
	(7, 7, 5, '2002'),
	(7, 8, 1, '2003'),
	(7, 9, 1, '2002'),
	(7, 10, 5, '2003'),
	(7, 11, 3, '2002'),
	(7, 12, 3, '2002'),
	(7, 13, 5, '2003'),
	(7, 14, 5, '2003'),
	(7, 15, 1, '2001'),
	(7, 16, 1, '2001'),
	(7, 17, 1, '2002'),
	(7, 18, 1, '2002'),
	(7, 19, 1, '2002'),
	(7, 20, 1, '2003'),
	(7, 21, 5, '2003'),
	(7, 22, 1, '2003'),
	(7, 23, 1, '2004'),
	(7, 24, 1, '2001'),
	(7, 25, 5, '2001'),
	(7, 26, 3, '2001'),
	(7, 27, 5, '2004'),

	(8, 1, 3, '2001'),
	(8, 2, 4, '2001'),
	(8, 3, 3, '2001'),
	(8, 4, 3, '2001'),
	(8, 5, 4, '2001'),
	(8, 6, 4, '2002'),
	(8, 7, 4, '2002'),
	(8, 8, 1, '2003'),
	(8, 9, 1, '2002'),
	(8, 10, 3, '2003'),
	(8, 11, 3, '2002'),
	(8, 12, 3, '2002'),
	(8, 13, 4, '2003'),
	(8, 14, 4, '2003'),
	(8, 15, 1, '2001'),
	(8, 16, 1, '2001'),
	(8, 17, 1, '2002'),
	(8, 18, 1, '2002'),
	(8, 19, 1, '2002'),
	(8, 20, 1, '2003'),
	(8, 21, 5, '2003'),
	(8, 22, 1, '2003'),
	(8, 23, 1, '2004'),
	(8, 24, 1, '2001'),
	(8, 25, 3, '2001'),
	(8, 26, 3, '2001'),
	(8, 27, 3, '2004'),

	(9, 1, 5, '2001'),
	(9, 2, 4, '2001'),
	(9, 3, 5, '2001'),
	(9, 4, 4, '2001'),
	(9, 5, 4, '2001'),
	(9, 6, 4, '2002'),
	(9, 7, 4, '2002'),
	(9, 8, 1, '2003'),
	(9, 9, 1, '2002'),
	(9, 10, 5, '2003'),
	(9, 11, 5, '2002'),
	(9, 12, 4, '2002'),
	(9, 13, 4, '2003'),
	(9, 14, 4, '2003'),
	(9, 15, 1, '2001'),
	(9, 16, 1, '2001'),
	(9, 17, 1, '2002'),
	(9, 18, 1, '2002'),
	(9, 19, 1, '2002'),
	(9, 20, 1, '2003'),
	(9, 21, 5, '2003'),
	(9, 22, 1, '2003'),
	(9, 23, 1, '2004'),
	(9, 24, 1, '2001'),
	(9, 25, 5, '2001'),
	(9, 26, 4, '2001'),
	(9, 27, 5, '2004');

/*1)	для указанной формы обучения посчитать количество студентов этой формы обучения;*/
SELECT g.tuitionForm, COUNT(*) AS tuitionFormCount
FROM students AS s
INNER JOIN studentsGroup AS g
ON s.groupId = g.Id
WHERE g.tuitionForm = 'дневная'
GROUP BY g.tuitionForm
ORDER BY g.tuitionForm;

/*2)	для указанной дисциплины получить количество часов и формы отчетности по этой дисциплине;*/
SELECT sp.Id, sub.subjectName, sp.hours, sp.examinationForm, sp.termNumber
FROM subjectsPlan AS sp
INNER JOIN subjects AS sub
ON sp.subjectId = sub.id
WHERE sub.subjectName = 'Физкультура'
ORDER BY sp.Id;

SELECT sub.Id, sub.subjectName, SUM(sp.hours) AS sumHours
FROM subjectsPlan AS sp
INNER JOIN subjects AS sub
ON sp.subjectId = sub.id
WHERE sub.subjectName = 'Физкультура'
GROUP BY sub.Id, sub.subjectName;

/*3)	найти N студентов с наилучшим средним баллом, 
вывести их в порядке убывания среднего балла, вывести их вместе с их средним баллом*/
CREATE VIEW university.avgStudentRating
AS SELECT s.Id, s.surname, s.firstName, s.patronymic, AVG(p.rating) AS avgRating
FROM progress AS p
INNER JOIN students AS s
ON p.studentId = s.Id
WHERE p.rating > 1
GROUP BY s.Id, s.surname, s.firstName, s.patronymic;

SELECT *
FROM avgStudentRating
ORDER BY avgRating DESC
LIMIT 5;

/*Запрос без VIEW*/
SELECT s.Id, s.surname, s.firstName, s.patronymic, AVG(p.rating) AS avgRating
FROM progress AS p
INNER JOIN students AS s
ON p.studentId = s.Id
WHERE p.rating > 1
GROUP BY s.Id, s.surname, s.firstName, s.patronymic
ORDER BY avgRating DESC
LIMIT 5; 

/*4)	найти студентов, которые должны получить стипендию после указанной сессии (год + семестр)*/
CREATE VIEW university.sessionResults
AS SELECT s.Id, s.surname, s.firstName, s.patronymic, sp.termNumber, p.passYear, p.Rating, sub.subjectName
FROM progress AS p
INNER JOIN students AS s
ON p.studentId = s.Id
INNER JOIN subjectsPlan AS sp
ON p.subjectPlanId = sp.Id
INNER JOIN subjects AS sub
ON sp.subjectId = sub.Id
WHERE sp.termNumber = 1;

SELECT *
FROM sessionResults
ORDER BY Id;

SELECT Id, surname, firstName, patronymic, termNumber, passYear
FROM sessionResults
WHERE Id NOT IN 
(
	SELECT Id
	FROM sessionResults
	WHERE (Rating < 4 AND Rating > 1) OR Rating = 0
	GROUP BY Id
)
GROUP BY Id, surname, firstName, patronymic, termNumber, passYear
ORDER BY Id;

/*Запрос без VIEW. Не стал приклеивать семестр и год сдачи, вроде заданию это не противоречит*/
SELECT Id, surname, firstName, patronymic
FROM students
WHERE Id NOT IN 
(
	SELECT studentId
	FROM progress
	INNER JOIN subjectsPlan AS sp
	ON subjectPlanId = sp.Id
	WHERE ((Rating < 4 AND Rating > 1) OR Rating = 0) AND sp.termNumber = 1
	GROUP BY studentId
)
GROUP BY Id, surname, firstName, patronymic
ORDER BY Id;

/*5)	найти дисциплину, по которой студенты лучше всего учатся*/
CREATE VIEW university.avgSubjectRating		
AS SELECT sub.Id, sub.subjectName, AVG(p.rating) AS avgRating
FROM progress AS p
INNER JOIN subjectsPlan AS sp
ON p.subjectPlanId = sp.Id
INNER JOIN subjects AS sub
ON sp.subjectId = sub.Id
WHERE p.Rating > 1
GROUP BY sub.Id, sub.subjectName;

SELECT *
FROM avgSubjectRating
WHERE avgSubjectRating.avgRating = 
(
	SELECT MAX(avgSubjectRating.avgRating)
    FROM avgSubjectRating
)
ORDER BY Id;

/*Запрос без VIEW*/
SELECT s.Id, s.subjectName, avgRating
FROM 
(
	SELECT sp.subjectId, AVG(rating) AS avgRating
	FROM progress
	INNER JOIN subjectsPlan AS sp
	ON subjectPlanId = sp.Id
	WHERE rating > 1
	GROUP BY sp.subjectId
) AS avgSR
INNER JOIN subjects AS s
ON avgSR.subjectId = s.Id
WHERE avgRating =
(
	SELECT MAX(avgSp.avgRating)
	FROM
	(
		SELECT AVG(rating) AS avgRating
		FROM progress
		INNER JOIN subjectsPlan AS sp
		ON subjectPlanId = sp.Id
		WHERE rating > 1
		GROUP BY sp.subjectId
	) AS avgSp
);

/*6)	найти студентов, учащихся выше среднего*/
SELECT *
FROM avgStudentRating
WHERE avgStudentRating.avgRating >
(
	SELECT AVG(avgStudentRating.avgRating)
    FROM avgStudentRating
)
ORDER BY avgStudentRating.avgRating DESC;

/*Запрос без VIEW*/
SELECT s.Id, s.surname, s.firstName, s.patronymic, AVG(p.rating) AS avgRating
FROM progress AS p
INNER JOIN students AS s
ON p.studentId = s.Id
WHERE p.rating > 1
GROUP BY s.Id, s.surname, s.firstName, s.patronymic
HAVING avgRating > 
(
	SELECT AVG(avgSR.avgR)
    FROM
    (
		SELECT studentId, AVG(rating) AS avgR
		FROM progress
        WHERE rating > 1
		GROUP BY studentId
    ) AS avgSR
)
ORDER BY avgRating DESC;