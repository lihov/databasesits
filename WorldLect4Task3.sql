USE world;

SELECT co.Name, c.Name, c.Population
FROM city AS c
INNER JOIN country AS co
ON co.Code = c.CountryCode
WHERE c.Population = (SELECT MIN(Population) FROM city
					WHERE CountryCode = c.CountryCode);