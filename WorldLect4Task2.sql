USE world;

SELECT city.Name, country.Name 
FROM city, country;

SELECT co.Code, co.Name, ci.Name 
FROM country AS co
INNER JOIN city AS ci
ON ci.ID = co.Capital
ORDER BY co.Name ASC;

USE shop;

DROP TABLE Orders;
DROP TABLE Clients;

CREATE TABLE Clients
(
	id INT AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE Orders
(
	id INT AUTO_INCREMENT,
    orderDate DATE NOT NULL,
	orderSum DECIMAL NOT NULL CHECK (orderSum >= 0),
	clientId INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY (clientId) REFERENCES Clients(id)
);

INSERT INTO Clients (name) 
VALUES ('Ivan'), ('Vasiliy'), ('Petr'), ('Yuriy');

INSERT INTO Orders (orderDate, orderSum, clientId)
VALUES 
	('2017-01-20', 1000.0, 1),
	('2017-01-21', 599.0, 1),
	('2017-01-22', 100.0, 2),
	('2017-01-23', 800.0, 3),
	('2017-01-24', 723.0, 2),
	('2017-01-20', 169.0, 3);

SELECT c.id, c.Name, COUNT(o.id) AS orderCount 
FROM clients AS c
LEFT JOIN orders AS o
ON c.id = o.clientId
GROUP BY c.id, c.Name
ORDER BY c.id;