USE world;

SELECT DISTINCT Continent 
FROM country 
WHERE Population >= 9500000 
ORDER BY Name ASC;

SELECT DISTINCT Continent 
FROM country 
WHERE Population >= 9500000 
ORDER BY Name ASC;

SELECT Name, SurfaceArea 
FROM country 
ORDER BY SurfaceArea 
DESC LIMIT 5;

SELECT Name, SurfaceArea 
FROM country 
ORDER BY 
SurfaceArea 
DESC LIMIT 5, 5;